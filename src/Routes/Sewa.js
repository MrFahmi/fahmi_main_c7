import React from 'react'
import SewaBody from '../Components/Body'
import Footer from '../Components/Footer'
import Navbar from '../Components/Navbar'

export default function Home() {
  return (
    <div>
        <Navbar/>
        <SewaBody/>
        <Footer/>
    </div>
  )
}
