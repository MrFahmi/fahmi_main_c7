import React from 'react'
import { Link } from 'react-router-dom'
import './Styles/Global.css'
import Facebook from '../Assets/images/footer/icon_facebook.svg'
import Instagram from '../Assets/images/footer/icon_instagram.svg'
import Mail from '../Assets/images/footer/icon_mail.svg'
import Twitter from '../Assets/images/footer/icon_twitter.svg'
import Twitch from '../Assets/images/footer/icon_twitch.svg'

export default function Footer() {
  return (
    <>
        <div className="footer text-center text-lg-start">
            <div className="container p-4">

            <div className="row">
                
                <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                <p className='text-dark text-left'>Lhokseumawe, Aceh</p>
                <Link className='text-decoration-none text-dark text-left' to='/'>fahmiarrosyid14@gmail.com</Link>
                <Link className='text-dark text-left' to='/'>0857-6603-1222</Link>
                </div>
                
        
                
                <div className="col-lg-3">
                <ul style={{ listStyle: 'none', paddingLeft: 0, textAlign: 'left' }}>
                    <li><Link to='/' style={{ textDecoration: 'none', color: 'black' }}>Our Services</Link></li>
                    <li><Link to='/' style={{ textDecoration: 'none', color: 'black' }}>Why Us</Link></li>
                    <li><Link to='/' style={{ textDecoration: 'none', color: 'black' }}>Testimonial</Link></li>
                    <li><Link to='/' style={{ textDecoration: 'none', color: 'black' }}>FAQ</Link></li>
                </ul>
                </div>
                
        
                
                <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                <p className="text-dark text-left">Connect With Us</p>
                <div className="d-flex">
                    <Link to='/' style={{ display: 'inline-table', marginRight: '16px' }}><img src={Facebook} alt="Facebook"/></Link>
                    <Link to='/' style={{ display: 'inline-table', marginRight: '16px' }}><img src={Instagram} alt="Instagram"/></Link>
                    <Link to='/' style={{ display: 'inline-table', marginRight: '16px' }}><img src={Twitter} alt="Twitter"/></Link>
                    <Link to='/' style={{ display: 'inline-table', marginRight: '16px' }}><img src={Mail} alt="Mail"/></Link>
                    <Link to='/' style={{ display: 'inline-table', marginRight: '16px' }}><img src={Twitch} alt="Twitch"/></Link>
                </div>
                </div>
                
        

                <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                <p className="text-dark text-left">Copyright FIFA Car Rental 2022</p>
                <Link className='navbar-brand text-light font-weight-bold' to='/'>FIFA Car Rental</Link>
                </div>
                
            </div>
            
            </div>
            
        </div>
    </>
  )
}
