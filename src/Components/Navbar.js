import React from 'react'
import './Styles/Global.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import pictMenu from './images/fi_menu.svg'

export default function Navbar() {
  return (
    <>
        <img id="home" src="/images/bg.svg" alt="" height="1" style={{ position: 'absolute', top: 0 }} />
        <nav className="navbar navbar-expand-lg navbar-light container-fluid" style={{ backgroundColor: '#F1F3FF', paddingRight: '5rem' }}>
        <a className='navbar-brand text-light' style={{ marginLeft: '3rem' }} href='/' >FIFA Car Rental</a>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav ms-auto">
            <a className='nav-item nav-link' href='#our-service-greet'>Our Service</a>
            <a className='nav-item nav-link' href='#whyus'>Why Us</a>
            <a className='nav-item nav-link' href='#testi'>Testimonial</a>
            <a className='nav-item nav-link' href='#faq' style={{ marginRight: '1rem' }}>FAQ</a>
            <a className='nav-item nav-link btn btn-success text-light font-weight-bold' href='/'>Register <FontAwesomeIcon icon={'pen-href-square'} /></a>
            </div>
        </div>
        </nav>

        <button className="btn-sidebar" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasWithBothOptions" aria-controls="offcanvasWithBothOptions"><img src={pictMenu} alt=""/></button>

        <div className="offcanvas offcanvas-end" data-bs-scroll="true" tabIndex="-1" id="offcanvasWithBothOptions" aria-labelledby="offcanvasWithBothOptionsLabel">
      <div className="offcanvas-header">
      </div>
      <div className="offcanvas-body">
        <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
          <a className='nav-item nav-link' href="#home">FCR</a>
          <a className='nav-item nav-link' href="#our-service">Our Service</a>
          <a className='nav-item nav-link' href="#whyus">Why Us</a>
          <a className='nav-item nav-link' href="#testi">Testimonial</a>
          <a className='nav-item nav-link' href="#faq">FAQ</a>
          <a className='nav-item nav-link btn btn-success sidebar text-light font-weight-bold' href="/">Register</a>
      </div>
    </div>

    </>
  )
}
